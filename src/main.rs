mod data_collectors;
use crate::data_collectors::SsmonConfig;
use failure::Error;
use std::fs::File;
use std::io::Read;
use std::{thread, time};

fn main() -> Result<(), Error> {
    env_logger::init();

    let mut config_file = File::open("ssmon.toml")?;
    let mut config_contents = String::new();
    config_file.read_to_string(&mut config_contents)?;
    let config: SsmonConfig = toml::from_str(&config_contents)?;
    let mut monitors = config.get_monitors()?;
    let mut conn = config.get_db_conn()?;
    monitors.create_tables(&mut conn)?;
    loop {
        monitors.poll(&mut conn)?;
        thread::sleep(time::Duration::from_secs(config.poll_interval));
    }
}
