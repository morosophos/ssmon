use crate::data_collectors::{Monitor, MonitorError, MonitorIndication};
use nvml_wrapper::enum_wrappers::device::TemperatureSensor;
use nvml_wrapper::NVML;
use postgres::types::ToSql;
use serde_derive::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct NvidiaConfig {
    device_index: u32,
}

#[derive(Debug)]
pub struct NvidiaIndication {
    gpu_utilization: i64,
    fan_speed: i64,
    temperature: i64,
    power_usage: i64,
    memory_used: f64,
}

pub struct NvidiaMonitor {
    nvml: NVML,
    config: NvidiaConfig,
}

impl MonitorIndication for NvidiaIndication {
    fn prepare_row(&self) -> Vec<Box<dyn ToSql>> {
        let mut res: Vec<Box<dyn ToSql>> = Vec::new();
        res.push(Box::new(self.gpu_utilization));
        res.push(Box::new(self.fan_speed));
        res.push(Box::new(self.temperature));
        res.push(Box::new(self.power_usage));
        res.push(Box::new(self.memory_used));
        res
    }
}

impl std::convert::From<nvml_wrapper::error::Error> for MonitorError {
    fn from(_nvml_error: nvml_wrapper::error::Error) -> MonitorError {
        MonitorError {
            error_message: "NVML Error".to_owned(),
        }
    }
}

impl NvidiaMonitor {
    pub fn new(config: NvidiaConfig) -> Result<Self, MonitorError> {
        let nvml = NVML::init()?;
        Ok(Self { nvml, config })
    }
}

impl Monitor for NvidiaMonitor {
    fn series_name(&self) -> &'static str {
        "nvidia"
    }
    fn columns(&self) -> &'static [(&'static str, &'static str)] {
        &[
            ("gpu_utilization", "BIGINT NOT NULL"),
            ("fan_speed", "BIGINT NOT NULL"),
            ("temperature", "BIGINT NOT NULL"),
            ("power_usage", "BIGINT NOT NULL"),
            ("memory_used", "DOUBLE PRECISION NOT NULL"),
        ]
    }
    fn poll(&mut self) -> Result<Box<dyn MonitorIndication>, MonitorError> {
        let device = self.nvml.device_by_index(self.config.device_index)?;
        Ok(Box::new(NvidiaIndication {
            gpu_utilization: device.utilization_rates()?.gpu as i64,
            fan_speed: device.fan_speed()? as i64,
            temperature: device.temperature(TemperatureSensor::Gpu)? as i64,
            memory_used: device.memory_info()?.used as f64,
            power_usage: device.power_usage()? as i64,
        }))
    }
}
