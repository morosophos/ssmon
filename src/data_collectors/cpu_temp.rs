use crate::data_collectors::{Monitor, MonitorError, MonitorIndication};
use postgres::types::ToSql;
use sensors::Sensors;
use serde_derive::Deserialize;

#[derive(Deserialize, Debug, Clone)]
struct SensorConfig {
    sensor: String,
    features: Vec<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct CpuTempConfig {
    main_temp: SensorConfig,
    fans: SensorConfig,
}

#[derive(Debug)]
pub struct CpuTempIndication {
    temp: f64,
    fans: Vec<f64>,
}

impl MonitorIndication for CpuTempIndication {
    fn prepare_row(&self) -> Vec<Box<dyn ToSql>> {
        let mut res: Vec<Box<dyn ToSql>> = Vec::new();
        res.push(Box::new(self.temp));
        res.push(Box::new(self.fans.clone()));
        res
    }
}

pub struct CpuTempMonitor {
    main_subfeature: sensors::Subfeature,
    fans_subfeatures: Vec<sensors::Subfeature>,
}

impl CpuTempMonitor {
    pub fn new(config: CpuTempConfig) -> Result<Self, MonitorError> {
        let mut main_subfeature = None;
        let mut fans_subfeatures = Vec::new();
        let sensors = Sensors::new();
        for chip in sensors.detected_chips(&config.main_temp.sensor)? {
            for feature in chip.into_iter() {
                if feature.name() == config.main_temp.features[0] {
                    main_subfeature = feature
                        .get_subfeature(sensors::SubfeatureType::SENSORS_SUBFEATURE_TEMP_INPUT);
                }
            }
        }
        for chip in sensors.detected_chips(&config.fans.sensor)? {
            for feature in chip.into_iter() {
                for n in &config.fans.features {
                    if n == feature.name() {
                        if let Some(subf) = feature
                            .get_subfeature(sensors::SubfeatureType::SENSORS_SUBFEATURE_FAN_INPUT)
                        {
                            fans_subfeatures.push(subf);
                        }
                        break;
                    }
                }
            }
        }
        if let Some(sf) = main_subfeature {
            Ok(Self {
                main_subfeature: sf,
                fans_subfeatures,
            })
        } else {
            Err(MonitorError {
                error_message: String::from("Can't find main temp feature"),
            })
        }
    }
}

impl std::convert::From<sensors::LibsensorsError> for MonitorError {
    fn from(_error: sensors::LibsensorsError) -> MonitorError {
        MonitorError {
            error_message: "Libsensors Error".to_owned(),
        }
    }
}

impl Monitor for CpuTempMonitor {
    fn series_name(&self) -> &'static str {
        "cpu_temp"
    }
    fn columns(&self) -> &'static [(&'static str, &'static str)] {
        &[
            ("temp", "DOUBLE PRECISION NOT NULL"),
            ("fans", "DOUBLE PRECISION[]"),
        ]
    }
    fn poll(&mut self) -> Result<Box<dyn MonitorIndication>, MonitorError> {
        let mut fans = Vec::new();
        for i in &self.fans_subfeatures {
            fans.push(i.get_value()?);
        }
        Ok(Box::new(CpuTempIndication {
            temp: self.main_subfeature.get_value()?,
            fans,
        }))
    }
}
