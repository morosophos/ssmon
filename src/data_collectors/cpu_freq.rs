use crate::data_collectors::{Monitor, MonitorError, MonitorIndication};
use postgres::types::ToSql;
use serde_derive::Deserialize;
use std::fs::File;
use std::io::Read;
use std::str;

#[derive(Deserialize, Debug, Clone)]
pub struct CpuFreqConfig {
    pub policy_number: u8,
}

#[derive(Debug)]
pub struct CpuFreqIndication {
    max: i64,
    min: i64,
    current: Vec<i64>,
    current_average: i64,
}

impl MonitorIndication for CpuFreqIndication {
    fn prepare_row(&self) -> Vec<Box<dyn ToSql>> {
        let mut res: Vec<Box<dyn ToSql>> = Vec::new();
        let current: Vec<i64> = self.current.iter().map(|x| x * 1000).collect();
        res.push(Box::new(self.max * 1000));
        res.push(Box::new(self.min * 1000));
        res.push(Box::new(current));
        res.push(Box::new(self.current_average * 1000));
        res
    }
}

pub struct CpuFreqMonitor {
    policy_number: u8,
    max: i64,
    min: i64,
}

impl std::convert::From<std::num::ParseIntError> for MonitorError {
    fn from(_error: std::num::ParseIntError) -> MonitorError {
        MonitorError {
            error_message: "Parse Int Error".to_owned(),
        }
    }
}

fn read_num_from_file(path: &str) -> Result<i64, MonitorError> {
    let mut f = File::open(path)?;
    let mut data = String::new();
    f.read_to_string(&mut data)?;
    let num = data.trim().parse()?;
    Ok(num)
}

enum PolicyType {
    MAX,
    MIN,
    CUR,
}

fn get_policy_file(n: u8, t: &PolicyType) -> String {
    let s = match t {
        PolicyType::MAX => "scaling_max_freq",
        PolicyType::MIN => "scaling_min_freq",
        PolicyType::CUR => "scaling_cur_freq",
    };
    format!("/sys/devices/system/cpu/cpufreq/policy{}/{}", n, s)
}

impl CpuFreqMonitor {
    pub fn new(policy_number: u8) -> Result<Self, MonitorError> {
        let max = read_num_from_file(&get_policy_file(0, &PolicyType::MAX))?;
        let min = read_num_from_file(&get_policy_file(0, &PolicyType::MIN))?;
        Ok(Self {
            policy_number,
            max,
            min,
        })
    }
}

impl Monitor for CpuFreqMonitor {
    fn series_name(&self) -> &'static str {
        "cpu_freq"
    }
    fn columns(&self) -> &'static [(&'static str, &'static str)] {
        &[
            ("max", "BIGINT NOT NULL"),
            ("min", "BIGINT NOT NULL"),
            ("current", "BIGINT[]"),
            ("current_average", "BIGINT NOT NULL"),
        ]
    }
    fn poll(&mut self) -> Result<Box<dyn MonitorIndication>, MonitorError> {
        let mut freqs = Vec::new();
        let mut s = 0;
        for i in 0..self.policy_number - 1 {
            let c = read_num_from_file(&get_policy_file(i, &PolicyType::CUR))?;
            s += c;
            freqs.push(c);
        }
        Ok(Box::new(CpuFreqIndication {
            min: self.min,
            max: self.max,
            current: freqs,
            current_average: s / (self.policy_number as i64),
        }))
    }
}
