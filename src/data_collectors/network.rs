use crate::data_collectors::{Monitor, MonitorError, MonitorIndication};
use postgres::types::ToSql;
use serde_derive::Deserialize;
use systemstat::{Platform, System};

#[derive(Deserialize, Debug, Clone)]
pub struct NetworkConfig {
    pub interface_name: String,
}

pub struct NetworkIndication {
    rx_bytes: usize,
    tx_bytes: usize,
}

impl MonitorIndication for NetworkIndication {
    fn prepare_row(&self) -> Vec<Box<dyn ToSql>> {
        let mut res: Vec<Box<dyn ToSql>> = Vec::new();
        res.push(Box::new(self.rx_bytes as i64));
        res.push(Box::new(self.tx_bytes as i64));
        res
    }
}

pub struct NetworkMonitor {
    system: System,
    interface_name: String,
}

impl NetworkMonitor {
    pub fn new(interface_name: String) -> Self {
        Self {
            system: System::new(),
            interface_name: interface_name,
        }
    }
}

impl Monitor for NetworkMonitor {
    fn series_name(&self) -> &'static str {
        "network"
    }
    fn columns(&self) -> &'static [(&'static str, &'static str)] {
        // TODO: better store for CPU loads maybe?
        &[
            ("rx_bytes", "BIGINT NOT NULL"),
            ("tx_bytes", "BIGINT NOT NULL"),
        ]
    }

    fn poll(&mut self) -> Result<Box<dyn MonitorIndication>, MonitorError> {
        let net = self.system.network_stats(&self.interface_name)?;
        let indication = NetworkIndication {
            rx_bytes: net.rx_bytes,
            tx_bytes: net.tx_bytes,
        };
        Ok(Box::new(indication))
    }
}
