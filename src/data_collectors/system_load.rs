use crate::data_collectors::{Monitor, MonitorError, MonitorIndication};
use postgres::types::ToSql;
use serde_derive::Deserialize;
use serde_json::{json, Value};
use systemstat::data::*;
use systemstat::{Platform, System};

#[derive(Deserialize, Debug, Clone)]
pub struct SystemLoadConfig {}

pub struct SystemLoadIndication {
    load_average: LoadAverage,
    cpu_load: Vec<CPULoad>,
    cpu_load_agg: CPULoad,
    memory: usize,
}

fn cpu_load_to_json(c: &CPULoad) -> Value {
    json!({
        "user": c.user,
        "nice": c.nice,
        "system": c.system,
        "interrupt": c.interrupt,
        "idle": c.idle,
        "iowait": c.platform.iowait
    })
}

pub struct SystemLoadMonitor {
    system: System,
    cpu_load_measurement: DelayedMeasurement<Vec<CPULoad>>,
    cpu_load_agg_measurement: DelayedMeasurement<CPULoad>,
}

impl std::convert::From<std::io::Error> for MonitorError {
    fn from(_error: std::io::Error) -> MonitorError {
        MonitorError {
            error_message: "IO Error".to_owned(),
        }
    }
}

impl SystemLoadMonitor {
    pub fn new() -> Result<Self, MonitorError> {
        let system = System::new();
        let cpu_load_measurement = system.cpu_load()?;
        let cpu_load_agg_measurement = system.cpu_load_aggregate()?;

        Ok(Self {
            system,
            cpu_load_measurement,
            cpu_load_agg_measurement,
        })
    }
}

impl MonitorIndication for SystemLoadIndication {
    fn prepare_row(&self) -> Vec<Box<dyn ToSql>> {
        let mut res: Vec<Box<dyn ToSql>> = Vec::new();
        res.push(Box::new(self.load_average.one));
        res.push(Box::new(self.load_average.five));
        res.push(Box::new(self.load_average.fifteen));
        res.push(Box::new(cpu_load_to_json(&self.cpu_load_agg)));
        res.push(Box::new(self.memory as i64));
        res
    }
}

impl Monitor for SystemLoadMonitor {
    fn series_name(&self) -> &'static str {
        "system_load"
    }
    fn columns(&self) -> &'static [(&'static str, &'static str)] {
        // TODO: better store for CPU loads maybe?
        &[
            ("load_avg_1", "REAL NOT NULL"),
            ("load_avg_5", "REAL NOT NULL"),
            ("load_avg_15", "REAL NOT NULL"),
            ("cpu_agg", "JSONB"),
            ("memory", "BIGINT NOT NULL"),
        ]
    }

    fn poll(&mut self) -> Result<Box<dyn MonitorIndication>, MonitorError> {
        let mem = self.system.memory()?;
        let indication = SystemLoadIndication {
            load_average: self.system.load_average()?,
            cpu_load: self.cpu_load_measurement.done()?,
            cpu_load_agg: self.cpu_load_agg_measurement.done()?,
            memory: mem.total.as_usize() - mem.free.as_usize(),
        };
        self.cpu_load_measurement = self.system.cpu_load()?;
        self.cpu_load_agg_measurement = self.system.cpu_load_aggregate()?;
        Ok(Box::new(indication))
    }
}
