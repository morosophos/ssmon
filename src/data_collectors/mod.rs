mod cpu_freq;
mod cpu_temp;
mod network;
mod nvidia;
mod system_load;

use crate::data_collectors::cpu_freq::{CpuFreqConfig, CpuFreqMonitor};
use crate::data_collectors::cpu_temp::{CpuTempConfig, CpuTempMonitor};
use crate::data_collectors::network::{NetworkConfig, NetworkMonitor};
use crate::data_collectors::nvidia::{NvidiaConfig, NvidiaMonitor};
use crate::data_collectors::system_load::{SystemLoadConfig, SystemLoadMonitor};
use core::fmt::Display;
use failure::Fail;
use postgres::types::ToSql;
use serde_derive::Deserialize;

pub struct Monitors {
    monitors: Vec<Box<dyn Monitor>>,
}

impl Monitors {
    pub fn create_tables(&self, conn: &mut postgres::Connection) -> Result<(), MonitorError> {
        for i in &self.monitors {
            i.create_table(conn)?;
        }
        Ok(())
    }

    pub fn poll(&mut self, conn: &mut postgres::Connection) -> Result<(), MonitorError> {
        for i in self.monitors.iter_mut() {
            let indication = i.poll()?;
            i.write(conn, &*indication)?;
        }
        Ok(())
    }
}

#[derive(Deserialize, Debug)]
pub struct SsmonConfig {
    database_url: String,
    pub poll_interval: u64,
    purge_older_than: u64,
    cpu_temp: Option<CpuTempConfig>,
    cpu_freq: Option<CpuFreqConfig>,
    nvidia: Option<NvidiaConfig>,
    system_load: Option<SystemLoadConfig>,
    network: Option<NetworkConfig>,
}

impl SsmonConfig {
    pub fn get_db_conn(&self) -> Result<postgres::Connection, MonitorError> {
        let conn =
            postgres::Connection::connect(self.database_url.as_str(), postgres::TlsMode::None)?;
        Ok(conn)
    }

    pub fn get_monitors(&self) -> Result<Monitors, MonitorError> {
        let mut res: Vec<Box<dyn Monitor>> = Vec::new();
        if let Some(nvidia_config) = &self.nvidia {
            res.push(Box::new(NvidiaMonitor::new(nvidia_config.clone())?));
        }
        if let Some(cpu_temp_config) = &self.cpu_temp {
            res.push(Box::new(CpuTempMonitor::new(cpu_temp_config.clone())?))
        }
        if let Some(cpu_freq_config) = &self.cpu_freq {
            res.push(Box::new(CpuFreqMonitor::new(
                cpu_freq_config.policy_number,
            )?));
        }
        if let Some(_) = &self.system_load {
            res.push(Box::new(SystemLoadMonitor::new()?))
        }
        if let Some(network_config) = &self.network {
            res.push(Box::new(NetworkMonitor::new(
                network_config.interface_name.clone(),
            )))
        }
        Ok(Monitors { monitors: res })
    }
}

#[derive(Debug)]
pub struct MonitorError {
    error_message: String,
}

impl std::convert::From<postgres::Error> for MonitorError {
    fn from(_io_error: postgres::Error) -> MonitorError {
        MonitorError {
            error_message: "DB Error".to_owned(),
        }
    }
}

impl Display for MonitorError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(f, "{}", self.error_message)
    }
}

impl Fail for MonitorError {}

pub trait MonitorIndication {
    fn prepare_row(&self) -> Vec<Box<dyn ToSql>>;
}

pub trait Monitor {
    fn series_name(&self) -> &'static str;
    fn columns(&self) -> &'static [(&'static str, &'static str)];

    fn write(
        &self,
        conn: &mut postgres::Connection,
        indication: &dyn MonitorIndication,
    ) -> Result<(), MonitorError> {
        let data = indication.prepare_row();
        let mut row = Vec::new();
        for i in 0..data.len() {
            row.push(&*data[i]);
        }
        let mut cols = String::new();
        let mut vals = String::new();
        cols.push_str("timestamp");
        vals.push_str("NOW()");
        let mut ix = 1;
        for (i, _) in self.columns() {
            cols.push_str(", ");
            cols.push_str(i);
            vals.push_str(&format!(", ${}", ix));
            ix += 1;
        }
        let sql = format!(
            "INSERT INTO {} ({}) VALUES ({}) ",
            self.series_name(),
            cols,
            vals
        );
        conn.execute(&sql, row.as_slice())?;
        Ok(())
    }

    fn create_table(&self, conn: &mut postgres::Connection) -> Result<(), MonitorError> {
        let mut coldefs = String::new();
        for (col, coltype) in self.columns() {
            coldefs.push_str(&format!(", \"{}\" {}", col, coltype));
        }
        let table_sql = format!(
            "CREATE TABLE IF NOT EXISTS {} (\"id\" SERIAL PRIMARY KEY, \"timestamp\" TIMESTAMP WITH TIME ZONE{})",
            self.series_name(),
            coldefs
        );
        let index_sql = format!(
            "CREATE INDEX IF NOT EXISTS {}_timestamp on {} (\"timestamp\")",
            self.series_name(),
            self.series_name()
        );
        conn.execute(&table_sql, &[])?;
        conn.execute(&index_sql, &[])?;
        Ok(())
    }

    fn purge(&self, conn: &mut postgres::Connection, days: u64) -> Result<(), MonitorError> {
        let sql = format!(
            "DELETE FROM \"{}\" WHERE timestamp < NOW() - interval '{} days'",
            self.series_name(),
            days
        );
        conn.execute(&sql, &[])?;
        Ok(())
    }

    fn poll(&mut self) -> Result<Box<dyn MonitorIndication>, MonitorError>;
}
